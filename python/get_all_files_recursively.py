# https://stackoverflow.com/questions/7201203/python-current-directory-in-an-os-walk
# https://stackoverflow.com/questions/18394147/recursive-sub-folder-search-and-return-files-in-a-list-python
import os
dir = os.getcwd()
for root, subFolder, files in os.walk(dir):
    for item in files:
        fileNamePath = str(os.path.join(root,item))
        print(fileNamePath)
