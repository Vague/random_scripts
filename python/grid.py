# vague
# proof of concept lettergrid
# make a framebuffer of letters

# https://www.geeksforgeeks.org/python-using-2d-arrays-lists-the-right-way/

import os
import time
cols,rows = os.get_terminal_size()
print("we are " + str(cols) + " x " + str(rows) )
time.sleep(1)


# todo 
# write a function that displays an image at a given coord in the framebuffer
# put the display refreshing loop in its own thread
# have another thread for user input
# some system for user control of displayed images


# ok first off we need a framebuffer
def clearfb():
    return [[0 for i in range(cols)] for j in range(rows)]


# theres a way to do this faster using screen command characters 
# once that's done you can scan the fb for changes and only update the changed cells
def display(fb):
    for col in fb:
        for row in col:
            print(row, end='')
    print()
    print()

fb = clearfb()
for b in range(rows):
    display(fb)
    fb = clearfb()
    time.sleep(.5)
    for a in range(cols):
        fb[b][a] = 1
    
print()


