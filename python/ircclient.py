# make the beginnings of an irc client
# tinyurl.com/zasuush

import os,sys
import time
import threading
import socket

global sendbuffer, recvbuffer, outfilehandle, infilehandle
sendbuffer = []
recvbuffer = []

user = "a b c d e"
nick = "blackbeard97"
ircdir = "irc"
outfilename = "out"

def masterrecv():
	# TODO try recving 4 bytes at a time and see if its \r\n
	# if it is, then send
	global sendbuffer, recvbuffer, sock, outfilehandle
	while True:
		data = sock.recv(1024)
		
		datastr = data.decode()
		datastr = datastr[:-2]
		print("got " + datastr)

		if (datastr[:4] == "PING"):
			print("got ping")
			sock.sendall(bytes(datastr.replace('I','O'), 'utf-8'))
			continue
		if (datastr[:5] == "ERROR"):
			print("error, quit")
			sys.exit()
	
		outfilehandle.write(datastr)	

def mastersend():
	global sendbuffer, recvbuffer, sock
	while True:
		if (len(sendbuffer) != 0):
			payload = sendbuffer.pop()
			print("pl " +payload)
			sock.sendall(bytes(payload, 'utf-8'))
		time.sleep(1)

def registerusernick():
	global sendbuffer
	msg = "USER " + user + "\n" + "NICK " + nick + "\r\n\n"
	print("initconn " + msg)
	sendbuffer.append(msg)	
	time.sleep(1)
	sendbuffer.append(msg)	
	# register twice. freenode registers the first time but rizon does not
	# maybe keep reregistering until you get the You may not reregister error


def threadnamedpipein():
	global sendbuffer
	while True:
		mynamedpipe = open('in')
		for line in mynamedpipe:
			print(line,end='')
			sendbuffer.append(line)	
		mynamedpipe.close()
		time.sleep(0.25)


# create io ctrl dirs
try:
	# irc dir
	os.makedirs(ircdir, exist_ok=True)
	os.chdir(ircdir)
	outfilehandle = open('out','a')
	# named pipe
	if (os.path.exists("in")):
		os.remove("in")
	os.mkfifo("in")
except BaseException as e:
    print("io setup failed: %s" % e)

	


# server connection
# Create a TCP/IP socket
global sock
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Connect the socket to the port where the server is listening
#server_address = ('irc.freenode.net', 6669)
server_address = ('irc.rizon.net', 6669)
#server_address = ('localhost', 9999) # local testing with netcat
print('connecting to {} port {}'.format(*server_address))
sock.connect(server_address)


# start threads
x = threading.Thread(target=mastersend)
x.start()
y = threading.Thread(target=masterrecv)
y.start()
z = threading.Thread(target=threadnamedpipein)
z.start()

time.sleep(5)
registerusernick()

while True:
	#data = sock.recv(265)
	#print('received {!r}'.format(data))
	time.sleep(0.50)
