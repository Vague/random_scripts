# vague 
# make a named pipe in python
# remade for python3
# https://www.tutorialspoint.com/How-to-create-and-use-a-named-pipe-in-Python

import os, sys
# Path to be created
path = "/tmp/namedpipe"
try:
    os.mkfifo(path)
except BaseException as e:
    print("Failed to create FIFO: %s" % e)
print( "Path is created")

while True:
	mynamedpipe = open(path) 
	for line in mynamedpipe:
		print(line,end='')
	mynamedpipe.close()
