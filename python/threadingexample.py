# https://pymotw.com/3/socket/tcp.html
# http://protofusion.org/wordpress/2009/08/irc-via-telnet/
# https://docs.python.org/3/tutorial/classes.html
# https://realpython.com/intro-to-python-threading/

import sys
import time
import threading

global sendbuffer 
sendbuffer = ""

def masterrecv():
	global sendbuffer 
	while True:
		sendbuffer = "payload"
		time.sleep(3.3)

def mastersend():
	global sendbuffer 
	while True:
		if (len(sendbuffer) != 0):
			print(sendbuffer)
			sendbuffer = ""
		time.sleep(2.196)


x = threading.Thread(target=mastersend)
x.start()

y = threading.Thread(target=masterrecv)
y.start()


while True:
	print("we see sendbuffer: " + sendbuffer) 
	time.sleep(0.50)
