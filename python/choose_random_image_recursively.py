# see get_all_images_recursively.py
import os,random
dir = os.getcwd()
images = []
for root, subFolder, files in os.walk(dir):
    for item in files:
        fileNamePath = str(os.path.join(root,item))
        if item.endswith((".jpg",".JPG",".jpeg",".JPEG",".png",".gif")):
            images.append(fileNamePath)

print("chosen image: " + str(random.choice(images)))
