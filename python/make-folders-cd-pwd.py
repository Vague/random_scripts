# vague
# notes for python making folders cd pwd etc

# https://www.geeksforgeeks.org/create-a-directory-in-python/
# https://stackoverflow.com/questions/12468022/python-fileexists-error-when-making-directory

# lots of interesting goodies
# https://docs.python.org/3/library/os.html

import os
mydir = "/tmp/testdir"
os.makedirs(mydir, exist_ok=True)

print(os.getcwd())
print(os.listdir())
os.chdir(mydir)
print(os.getcwd())

print("os name " + os.name)

if not( os.path.exists("testfile") ) :
	os.mknod("testfile")
else:
	os.remove("testfile")

print(os.listdir())

