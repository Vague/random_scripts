# see choose_random_image_recursively.py
# see setwin10wallpaper.py
import os,random
dir = os.getcwd()
images = []
for root, subFolder, files in os.walk(dir):
    for item in files:
        fileNamePath = str(os.path.join(root,item))
        if item.endswith((".jpg",".JPG",".jpeg",".JPEG",".png",".gif")):
            images.append(fileNamePath)

image = random.choice(images)
print("chose: " + image)

# only works on win10 x64 as of 2021/05/19 yyyymmdd
import ctypes
ctypes.windll.user32.SystemParametersInfoW(20, 0, image , 0)
