# vague 
# Thu Mar  7 10:37:35 AM EST 2024
# show the top bar battery percentage. i couldnt find it in settings and the option has been removed from gnometweaks, which is baffling. gnome has been frustrating for over a decade now. it might as well be only confiurable through `gsettings set`
# https://askubuntu.com/questions/946711/how-do-i-show-battery-percentage-in-gnome-panel-without-using-any-extension
gsettings set org.gnome.desktop.interface show-battery-percentage true
