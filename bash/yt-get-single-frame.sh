# vague
# get a current frame from a live youtube video
# requires youtube-dl, ffmpeg, curl 

url="https://www.youtube.com/watch?v=zQ3GnJhI_-4" # weather channel
url="https://www.youtube.com/watch?v=ulEKv7LJDxc" # parking lot
url="https://www.youtube.com/watch?v=k0QmUxhGD5s" # train tracks
url="https://www.youtube.com/watch?v=LQpGUOJtdnY" # tourist mountain
url="https://www.youtube.com/watch?v=w_Ma8oQLmSM" # big news
url="https://www.youtube.com/watch?v=HQ5DsGka2qo" # Ober Gatlinburg
url="https://www.youtube.com/watch?v=Ah1V-dHTbhg" # city view

pls=$(youtube-dl -g "$url")
echo pls $pls
seg=$(curl "$pls" | tail -1)
echo get $seg

rm -rf /tmp/getframe/
mkdir -p /tmp/getframe/
pushd /tmp/getframe/
curl -O  "$seg"

ffmpeg -ss 0.0 -i seg.ts -vframes 1 -s 1920x1080 -f image2 imagefile.jpg

xviewer imagefile.jpg
