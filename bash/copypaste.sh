# vague
# use xdotool to type whatever is in the clipboard. 
# useful for bad websites that disable pasting, causing you to use a short easy to crack password for "security"
# requires an x11 based display or maybe you could use the compatibility program. 
# unfortunately i couldnt get anything similar to work on wayland

xdotool type "$(xclip -o -selection clipboard)"
