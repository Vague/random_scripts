#!/usr/bin/env bash
# run things using the gpu
# https://starbeamrainbowlabs.com/blog/article.php?article=posts%2F436-running-application-on-nvidia.html
export __NV_PRIME_RENDER_OFFLOAD=1;
export __GLX_VENDOR_LIBRARY_NAME=nvidia;
export DRI_PRIME=1

$@;
