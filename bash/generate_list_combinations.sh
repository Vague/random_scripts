#!/bin/bash
# vague Sat Nov 11 04:01:51 PM EST 2023
# I was struggling with this problem yesterday, generating all combinations of a list in bash. The normal way to do it involves recursion, and I tried it using bash arrays and they dont play nicely as function args (thats where shift is needed ). It was late and I was too tired to try doing it with lists instead of arrays, which probably would have worked way better. It (much later) occurred to me that binary numbers are unique combinations of zeros and ones so I used them to mask out elements in the list and do my combinations that way. It worked after some finagling.

generate_combinations(){
	# 1 = list of words
	# Return all combinations (order doesnt matter) of a list of words. 
	# If order matters, run this twice and reverse the output of the second run.

	list="$1"
	len=$(echo "$list" | wc -w)
	len_combination=0

	dec2bin(){
	# 1 = decimal number
	# output = unpadded binary representation
	# https://stackoverflow.com/questions/55239476/awk-convert-decimal-to-binary
		awk '
		function d2b(d,  b) {
		      while(d) {
			  b=d%2b
			  d=int(d/2)
		      }
		      return(b)
		}
		BEGIN {
		    print d2b('"$1"')
		}'
	}

	mask_list(){
	# 1 = mask in dec
	# 2 = list of words
	# output = list masked by binary 
		# reverse binary input so we dont have to care about padding
		mask=$( echo "$1" | rev )
		list="$2"
		len=$(echo "$list" | wc -w)
		count=0
		for word in $list; do
			#echo word $word, count $count, bin $mask
			check_mask=${mask:$count:1}
			[ "$check_mask" = '1' ] && echo "$word"
			count=$((count+1))
		done
	}

	
	# Quit once you find the end of the combinations, where the mask is all 1's and returns the original list. 
	# There's some way to calculate the exact number of iterations it needs, but I ran out of time. 
	# https://en.wikipedia.org/wiki/Combination
	count=0
	while [ ! "$len_combination" -ge "$len" ] ; do
		bin_mask="$(dec2bin "$count")"
		combination=$(mask_list "$bin_mask" "$list") 
		# Don't quote this, even if shellcheck tells you to, or it makes the output unusable
		echo $combination
		len_combination=$( echo "$combination" | wc -w )
		count=$((count+1))	
	done
}


list='aa bb cc dd ee ff gg hh '
generate_combinations "$list"
