#!/bin/bash
# https://grepjason.sh/2022/creating-my-own-dynamic-dns-using-porkbun-api
# grab wan ip from canhazip and set variable to output
output=$(curl http://canhazip.com)
# display wan ip for troubleshooting
echo "$output";
# wait 2 seconds
sleep 2s

# update dns ip of sub.domain.com via porkbun api
ping -c 1 ipaddress1
ping -c 1 ipaddress2
curl --header "Content-type: application/json" --request POST --data '{"secretapikey":"sk1_123456","apikey":"pk1_123456","content":"'$output'"}' https://porkbun.com/api/json/v3/dns/editByNameType/mycooldomainname.tar/A/mycoolsubdomain

ping -c 1 ipaddress1
ping -c 1 ipaddress2
