# get a fortune from helloacm.com
# faster than setting up fortune on some systems

wget -q -O - https://helloacm.com/fortune/? | grep -B 999 "Get Random" | grep -A 999 "Random Adage" | grep -v '<input class' | grep -v '<legend>' | sed -e 's/<pre>//' -e 's/<\/pre>//' | grep -v ^$
