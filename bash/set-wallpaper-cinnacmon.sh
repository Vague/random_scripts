# vague
# set wallpaper from commandline on linux running cinnamon
# https://unix.stackexchange.com/questions/59653/change-desktop-wallpaper-from-terminal
# drawbacks - centers, doesnt stretch
gsettings set org.cinnamon.desktop.background picture-uri  "file:///$@"
