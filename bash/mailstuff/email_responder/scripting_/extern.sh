#!/bin/bash
# extern.sh
# external component of the email responder dispatch phase 3

# message_file expects to be a full valid path
message_file="$1"
#echo externaller with "$message_file"

#cat $message_file

# ========================================================== send wrapper
sendr(){
	echo sending mail to "$2"
	echo 'cat - | mutt -s '"$1" "$2"

	[ ! -e /tmp/lastsend ] && echo 0 > /tmp/lastsend 
	#		now	 - 	then   
	while [ ! "$(( $(date +%s) - $(cat /tmp/lastsend) ))" -gt 5 ] ; do
		echo waiting for send timeout clear $(date +%s) vs $(cat /tmp/lastsend)
		sleep 1
	done

	cat - | mutt -s "$1" "$2"
	
	date +%s > /tmp/lastsend
}

# ========================================================== command processor
proccom(){
	echo this is proccom. args $@
	grep '!ping' <<< "$@" -q && {
		echo ping pong!
		echo pong | sendr 'pong' "$line_from"
		return 0
	}
	grep '!w' <<< "$@" -q && {
		echo 'weather! cmd, args ' $@
		bash scripting/cmd_weather.sh "$@" | sendr 'weather response' $line_from
		return 0
	}
	
}

# ========================================================== read command input
line_from=$(grep '^From:' "$message_file" | sed -e 's/.*<//' -e 's/>.*//' -e 's/From://')
line_subject=$(grep '^Subject:' "$message_file" | sed -e 's/.*<//' -e 's/>.*//')

echo from $line_from
if [ -z "$line_from" ] ; then
	echo no from
	exit
fi

echo do we have a subject?
if [ ! -z "$line_subject" ] ; then
	echo got a subject "$line_subject"
else
	echo no subject line
fi

cmd_subj=$(echo "$line_subject" | grep '![a-z]*' -o )
if [ ! -z "$cmd_subj" ] ; then
	echo subject command is "$cmd_subj"
	proccom "$cmd_subj"
else
	echo no subject cmd
fi
cmd_body=$(cat "$message_file" | grep -v "Subject:" | grep '![a-z].*' -o )
if [ ! -z "$cmd_body" ] ; then
	echo body command is "$cmd_body"
	proccom "$cmd_body"
else
	echo no body cmd
fi

#
#	# $1 = message
#	# $2 = from
#	# $3 = subject
#	#externaller "$cmd_subj" "$1" "$2"
#
## new externaller: just give me the email and ill process it
#
#	line_from=$(grep '^From:' "$message" | sed -e 's/.*<//' -e 's/>.*//')
#	line_subject=$(grep '^Subject:' "$message" | sed -e 's/.*<//' -e 's/>.*//')
#	echo from "$line_from" 
#	echo subject "$line_subject"
#
##externaller(){
##	#echo externaller with "$@"
##	$externaller "$@"
##}
##
##dispatch_to_external(){
##	# $1 = message
##	# $2 = from
##	# $3 = subject
##	#echo dispatch_to_external got "$@"
##	# get commands
##	# in subject?
##	#echo subject "$3"
##	echo "$3" | grep '![a-z]...' -q && {
##		#echo got a command in the subject
##		cmd_subj=$(echo "$3" | grep '![a-z]...*' -o )
##		echo subject command is "$cmd_subj"
##		externaller "$cmd_subj" "$1" "$2"
##	}
##	#echo body "$(cat $1)" cat "$1" | grep -v "Subject:" | grep '![a-z]...' -q && {
##		#echo got a command in the body
##		cmd_body=$(cat "$1" | grep -v "Subject:" | grep '![a-z]...*' -o )
##		echo body cmd is "$cmd_body"
##		externaller "$cmd_body" "$1" "$2"
##	}
##			
##}
#
#
#echo 
#echo 
#echo extern.sh with \'"$1"\' \'"$2"\' \'"$3"\'
#
#cmd_body="$( sed -e 's/\ .*//g' <<< $1 )"
#echo got "$cmd_body"
#
#line_from="$( sed -e 's/.*\ //g' <<< $3 )"
#
#case "$cmd_body" in
#	"!ping")
#		echo ping pong\!
#		echo sending to $line_from
#		echo pong | mutt -s 'pong' $line_from
#	;;
#	*) 
#		echo bad cmd
#	;; 
#esac
#
