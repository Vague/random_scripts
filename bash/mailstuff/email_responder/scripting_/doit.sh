externaller="bash scripting/extern.sh "
#externaller="echo extern: "

# ========================================================== dispatch stage 0

# onstartup, recover the previous newest mail, or find newest one
get_newest(){ cat /tmp/newest | tr -d '\n' ; }
newest=$( get_newest )
newest=$(find -name *${newest} )
#echo got $newest
if [ ! -e "$newest" ] ; then
	echo find new old newest
	newest=$(find -type f -name *boneyexpress* | sort -n | tail -n1)
	newest=$(basename $newest)
	echo $newest > /tmp/newest
	echo old new newest: $newest
fi
echo start newest $newest 

# find the newest message(s)
# this calls $externaller on every mail. 
# as it does, it saves its place so it can resume
doit(){
#echo doing it
mynewest=$(cat /tmp/newest | tr -d '\n' )
find -name *boneyexpress* -type f | sort -n | grep $mynewest -A 99999 | grep -v $mynewest \
	|  while read -r message ; do
	echo found $message
	$externaller "$message"
	basename "$message" > /tmp/newest
done
}


while : ; do
	# just kick off one time
	doit

	# wait for and process new messages. main lives here
	for a in {0..10} ; do logger 'clear journal LOL' ; done
	timeout 19 journalctl -f |  while read -r line ; do 
		grep 'message UID [0-9]*' -o <<< $line || continue
		echo got $line 
		doit
		echo process top level concludes
	done
	
	sleep 1
done


