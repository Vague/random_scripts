#!/bin/bash
# weather.sh 
# the weather command

#echo weather "$@"
#echo reading cmd from '$1'

exe_weather=/root/ansiweather/ansiweather 

cmd_args=$(echo "$1" | sed -e 's:.*\!\w[^ ]*::' )
echo cmd_args \'"$cmd_args"\' >&2

if [ -z "$cmd_args" ] ; then
	#echo empty, use defaults
	$exe_weather -l jackson,tn,us -u imperial -F -a false -s true -d true | sed -e 's/Sat/\n&/' -e 's/Sun/\n&/' -e 's/Mon/\n&/' -e 's/Tue/\n&/' -e 's/Wed/\n&/' -e 's/Thu/\n&/' -e 's/Fri/\n&/' | sed -e 's/-\s$//'	
	echo ---
	$exe_weather -l nashville,tn,us -u imperial -F -a false -s true -d true | sed -e 's/Sat/\n&/' -e 's/Sun/\n&/' -e 's/Mon/\n&/' -e 's/Tue/\n&/' -e 's/Wed/\n&/' -e 's/Thu/\n&/' -e 's/Fri/\n&/' | sed -e 's/-\s$//'	
	echo ---
	$exe_weather -l cookeville,tn,us -u imperial -F -a false -s true -d true | sed -e 's/Sat/\n&/' -e 's/Sun/\n&/' -e 's/Mon/\n&/' -e 's/Tue/\n&/' -e 's/Wed/\n&/' -e 's/Thu/\n&/' -e 's/Fri/\n&/' | sed -e 's/-\s$//'	
	echo ---
	$exe_weather -l kingston,tn,us -u imperial -F -a false -s true -d true | sed -e 's/Sat/\n&/' -e 's/Sun/\n&/' -e 's/Mon/\n&/' -e 's/Tue/\n&/' -e 's/Wed/\n&/' -e 's/Thu/\n&/' -e 's/Fri/\n&/' | sed -e 's/-\s$//'	
	echo ---
	$exe_weather -l knoxville,tn,us -u imperial -F -a false -s true -d true | sed -e 's/Sat/\n&/' -e 's/Sun/\n&/' -e 's/Mon/\n&/' -e 's/Tue/\n&/' -e 's/Wed/\n&/' -e 's/Thu/\n&/' -e 's/Fri/\n&/' | sed -e 's/-\s$//'	
else 
	#echo you have args\! pass to executable
	#echo got "$cmd_args"
	$exe_weather $cmd_args
fi
