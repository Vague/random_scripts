#!/bin/bash
# extern.sh
# external component of the email responder dispatch phase 3

	# $1 = message
	# $2 = from
	# $3 = subject
	#externaller "$cmd_subj" "$1" "$2"
echo 
echo 
echo extern.sh with \'"$1"\' \'"$2"\' \'"$3"\'

cmd_body="$( sed -e 's/\ .*//g' <<< $1 )"
echo got "$cmd_body"

line_from="$( sed -e 's/.*\ //g' <<< $3 )"

case "$cmd_body" in
	"!ping")
		echo ping pong\!
		echo sending to $line_from
		echo pong | mutt -s 'pong' $line_from
	;;
	*) 
		echo bad cmd
	;; 
esac

