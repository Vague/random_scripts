externaller="bash ./extern.sh"
# ========================================================== dispatch stage 1

externaller(){
	#echo externaller with "$@"
	$externaller "$@"
}

dispatch_to_external(){
	# $1 = message
	# $2 = from
	# $3 = subject
	#echo dispatch_to_external got "$@"
	# get commands
	# in subject?
	#echo subject "$3"
	echo "$3" | grep '![a-z]...' -q && {
		#echo got a command in the subject
		cmd_subj=$(echo "$3" | grep '![a-z]...*' -o )
		echo subject command is "$cmd_subj"
		externaller "$cmd_subj" "$1" "$2"
	}
	#echo body "$(cat $1)" cat "$1" | grep -v "Subject:" | grep '![a-z]...' -q && {
		#echo got a command in the body
		cmd_body=$(cat "$1" | grep -v "Subject:" | grep '![a-z]...*' -o )
		echo body cmd is "$cmd_body"
		externaller "$cmd_body" "$1" "$2"
	}
			
}
# ========================================================== dispatch stage 0
# TODO
# redo this
# sort in chronologically
# log the last one for new starting point

# save the last message, which is also a timestamp. 
# if any come in, read all the messages after that through sort and grep -A, 
# then save the last message as the newest timestamp
newest=$(find -type f | grep boneyexpress | sort -n | tail -n1)
newest=$(basename $newest)
echo $newest > /tmp/newest
echo start newest $newest 
get_newest(){ cat /tmp/newest | tr -d '\n' ; }

doit(){
echo doing it
find -type f | grep boneyexpress | sort -n | grep $(get_newest) -A 99999 | grep -v $(get_newest) |  while read -r message ; do
	echo found $message
	line_from=$(grep '^From:' "$message" | sed -e 's/.*<//' -e 's/>.*//')
	line_subject=$(grep '^Subject:' "$message" | sed -e 's/.*<//' -e 's/>.*//')
	echo from "$line_from" 
	echo subject "$line_subject"
	dispatch_to_external "$message" "$line_from" "$line_subject"
	basename "$message" > /tmp/newest
done
}

# wait for and process new messages. main lives here
for a in {0..10} ; do logger 'clear journal LOL' ; done
journalctl -f |  while read -r line ; do 
	grep 'message UID [0-9]*' -o <<< $line || continue
	echo got $line 
	doit
done



