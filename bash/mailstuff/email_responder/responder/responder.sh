cd /home/vague/email-backup/gmail/inbox/new/ || exit
emcsavefile=/tmp/oldemc
emailcount=$(ls -t | wc -l)
logfile=/tmp/action.log

# incase we get emails when not running
if [ -e $emcsavefile ] ; then
	oldemc=$(cat $emcsavefile)
else
cmd 	echo $emailcount > $emcsavefile
	oldemc=$emailcount
fi

# TODO check if the newest mail and the last newest mail have the same name, instead 

mystat() {
echo $@ > /tmp/mystat.email
}
	
while : ; do
	mystat "running"
	# are there any new emails?
	emailcount=$(ls -t | wc -l)
	if [ "$emailcount" = "$oldemc" ] ; then
		#echo no change
		mystat "no change"
		sleep 3
		continue
	fi

	mystat "how many new emails?"
	
	# how many new emails are there?
	numberofnewmsg=$(( $emailcount  - $oldemc))
	echo found $numberofnewmsg new messages..
	
	[ "$numberofnewmsg" = "" ] && { echo bad msgs number \"$numberofnewmsg\" ; continue ; }
	[ "$numberofnewmsg" -gt 30 ] && { echo bad msgs number \"$numberofnewmsg\" ; continue ; }
	[ "$numberofnewmsg" -lt 0 ] && { 
		echo bad msgs number \"$numberofnewmsg\", resetting
		oldemc=$emailcount
		echo $emailcount > $emcsavefile
		continue 
	}
	[ "$numberofnewmsg" -gt 0 ] || { echo bad msgs number \"$numberofnewmsg\" ; continue ; }

	

	count=0
	# process each new email 
	ls -t 2>/dev/null | head -n $numberofnewmsg	| while read line ; do	
		mystat "processing all the email"
		count=$((count +1))
		echo found $line
		echo retr newest email..
		grep -A 99 From: $line
		
		# get From:
		fromline=$(grep From:\ .* $line | grep '[^\ ]*@[^\ ]*' -o | sort -u | sed -e 's/<//' -e 's/>//')
		# get Subject:
		subjectline=$(grep Subject:\ .* $line | sed -e 's/Subject: //' )
	
		echo looks like its subject $subjectline from $fromline
		
		# commands
		## !zoop
		grep '!zoop' $line -q && { 
		mystat "!zoop"
			echo GOT ZOOP
			echo EMAIL ZOOP BACK
			echo zoop | mutt -s "Re: $subjectline" $fromline
			echo $(date) from $from zoop >> $logfile
		} # can fork this, but not necessary
		## !weather location
		grep '!weather' $line -q && { 
		mystat "!weather"
			# get the line
			cmd=$(grep '!weather' $line )
			echo GOT weather line $cmd
			echo $cmd | sed -e 's/.*weather//'
			( for a in jackson cookville kingston knoxville ; do ansiweather -l $a,TN,US -u imperial -F -s true -afalse ; done ) | sed -e 's/Fri/\n&/g' -e 's/Sat/\n&/g' -e 's/Sun/\n&/g' -e 's/Mon/\n&/g' -e 's/Tue/\n&/g' -e 's/Wed/\n&/g'  -e 's/Thu/\n&/g' -e 's/-//g' | mutt -s "result" $fromline
		#	( echo jackson $(curl 2>/dev/null wttr.in/Jackson+tn?format=2) ; echo nashville $(curl 2>/dev/null wttr.in/nashville+tn?format=2) ; echo knoxville $(curl 2>/dev/null wttr.in/knoxville+tn?format=2) ) | mutt -s "Re: $subjectline" $fromline
			echo $(date) from $from weather >> $logfile
		}
	
		## !cmd 
		grep '!cmd' $line -q && { 
		mystat "!cmd"
			# get the line
			cmd=$(grep '!cmd' $line | sed -e 's/!cmd//' -e 's/Subject:*//' -e 's/Re:*//')
			echo running $cmd
			( $cmd ) 2>&1 | mutt -s result $fromline
			
			echo $(date) from $from cmd $cmd >> $logfile
		} 

		mystat "finishing"
		echo waiting 5 between emails
		echo processed $count of $numberofnewmsg emails
		
		sleep 5
		# TODO put a check, if we process more emails than exist in #numberofnewmsg then stop
	done
	
	oldemc=$emailcount
	echo $emailcount > $emcsavefile
	sleep 3
done


