# vague Fri Jul 28 02:08:47 PM EDT 2023
# ================================================================================
# functions
# ================================================================================

# just make it a bit nicer
trap wait EXIT

topdir=$PWD
already_downloaded(){
# $1 is youtube url like https://www.youtube.com/watch?v=GewTpFEg44Y
	echo ----
	echo got "$1"
	# add more slugs if have different site urls or url types

	# https://www.youtube.com/watch?v=Ts8w1Cjglww
	ytslug=$(grep 'youtube.com/watch?v=' <<< $1 | cut -d = -f2)
	if [ ! -z "$ytslug" ] ; then
		echo searching for $ytslug	
		find $topdir -name "*$ytslug*" 2>/dev/null | grep . && {
			echo found it, return 0, skipping
			return 0
		}
	fi

	# https://youtu.be/qZXdZwsx64I
	ytslug2=$(grep 'youtu.be/' <<< $1 | cut -d / -f4) 
	if [ ! -z "$ytslug2" ] ; then
		echo searching for $ytslug2	
		find $topdir -name "*$ytslug2*" 2>/dev/null | grep . && {
			echo found it, return 0, skipping
			return 0
		} 
	fi
	# didnt find it, return 1
	return 1
}

thread_limit=2
d="bash /home/vague/media_share/ytdlp.sh"
dlr="$d -x --sponsorblock-remove all "
dlrv="$d --sponsorblock-remove all "
downloader="$dlr"


mylog(){ echo "$@" >> /tmp/mylog.txt ; }

dl(){
	already_downloaded "$@" && return 0
	num_background_jobs=$(jobs -pr | wc -l )	
	if [ "$num_background_jobs" -ge $thread_limit ] ; then
		echo background job limit reached
		wait -n
		echo ready
	fi
	echo "$(date) starting + $@"
	$downloader "$@" &
}

# next
next() {
	mkdir $1
	pushd $1
	if [ ! -z "$2" ] ; then
		echo "$2" >> note.txt
	fi
}

# ready next
rn(){ 
	popd 
	next "$@"

}
# ================================================================================
# download stuff
# ================================================================================



rn royalty_free_music
dl https://youtu.be/qZXdZwsx64I
dl https://youtu.be/qZXdZwsx64I
dl https://youtu.be/qZXdZwsx64I

# download this one with video 
rn royalty_free_music_with_video
downloader=$dlv
dl https://youtu.be/qZXdZwsx64I
# next things downloaded without video
downloader=$dl


