# read man bash!
# go flowchart this
[ ! -z $1 ] && {
	myname=$1
} || {
	myname="miner1"
}
#fs=$(mktemp)
#fs=/tmp/mystatefile
# todo put vars in a vars folder so we can blow them away all at once
wdir=$(pwd)
vardir=vars
mkdir -p $vardir
flog=$wdir/log.txt
fs=$wdir/$vardir/${myname}_var_currentstate
fvar_prevstate=$wdir/$vardir/${myname}_var_prevstate
fvar_energy=$wdir/$vardir/${myname}_var_energy
fvar_gold=$wdir/$vardir/${myname}_var_gold
log(){
	echo $@ 
	echo $@ >> $flog
}
func_writefvar(){ echo $1 > $2 ; }
func_modfvar(){ #todo caching
	# $1 = fvar (path to var file)
	# $2 = number to add (can be negative)
	#echo func_modfvar $@
	local temp
	temp=$(cat $1)
	#echo have $temp temp
	temp=$(( temp + $2))
	#echo temp now $temp
	#echo $temp > $1
	func_writefvar $temp $1
}
func_getfvar(){ #todo caching
	# $1 = fvar(path to var file)
	cat $@
}
func_getrand(){
	local roll
	roll=$((RANDOM % $1))
	echo getrand rolled $roll >&2
	echo $roll
}
trans_state(){
	echo transition state $1
	#echo $1 > $fs
	func_writefvar $1 $fs
	# TODO only allow known states?
}
state_start(){
	log "starting $myname"
#	echo 5 > $fvar_energy
#	echo 0 > $fvar_gold
	func_writefvar 0 $fvar_gold
	func_writefvar 5 $fvar_energy
	#TODO now go to the mine
	trans_state stateflag_mining
}
state_mining(){
	log "mining $myname"
	# TODO 
	# add random element to gold incrememnt etcetc
	# decrement energy
	func_modfvar "$fvar_energy" "-1"
	echo have energy $(func_getfvar $fvar_energy)
	# increment gold
	func_modfvar "$fvar_gold" "$(func_getrand 5)"
	echo have gold $(func_getfvar $fvar_gold)

	[ $(func_getfvar $fvar_energy) -le 0 ] && {
		echo need goto recharge state
		trans_state stateflag_recharge	
	}
}
state_recharge(){
	echo state_recharge
}
echo working with $fs
#[ ! -e $fs ] && trans_state stateflag_start
st=$(cat $fs)
case $st in
	"stateflag_start") state_start ;;
	"stateflag_mining") state_mining ;;
	"stateflag_recharge") state_recharge ;;
	*) trans_state stateflag_start ;;
esac
