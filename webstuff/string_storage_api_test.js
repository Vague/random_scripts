// https://www.geeksforgeeks.org/routing-in-node-js/
var http = require('http');
var dict = {};
http.createServer(function (req, res) {
	console.log('looking at ' + req.connection.remoteAddress);
	res.writeHead(200, {'Content-Type': 'text/html'}); 
	var url = req.url;
	var args = url.split('=');
	// add keys
	if (args.length === 2){
		console.log('set key ' + args[0] + ' arg '  + args[1]);
		// add item
		dict[args[0]] = args[1];
		// nuke dict if it has too many entries
		console.log(Object.keys(dict).length)
		if (Object.keys(dict).length > 2000){
			dict = {};
		}
	} else if (args.length === 1) {
	// dump all keys
		if(url ==='/dumpall999') {
			var ret = "";
			for (const [key, value] of Object.entries(dict)) {
				//console.log(key, value);
				ret += key + " " + value + "<br>\n";
				res.write(ret); 
			}
		} else {
	// retrieve single keys
			console.log('req key ' + args[0])
			var retkey = dict[args[0]];
			if (typeof(retkey) === 'undefined'){
				retkey = Date.now();
			}
			console.log(retkey.toString());
			res.write(retkey.toString());
		}
			
	}

       res.end(); 
}).listen(3000, function() {
	// The server object listens on port 3000
	console.log("server start at port 3000");
});
