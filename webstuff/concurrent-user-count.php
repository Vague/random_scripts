<?php  
#echo 'hello User IP Address - '.$_SERVER['REMOTE_ADDR'];  
#echo 'Current script user: ' . get_current_user();

# TODO test for possible conflicts with multiple users
# TODO clean up the fopen calls for file_get/put

$filename = '/tmp/usercount';
$timestamp = '/tmp/ts';
$oldcount = '/tmp/oldcount';

# incr usercount, number of lines in the file
$fp = fopen($filename,'a+');
fwrite($fp,'a');
fclose($fp);

# read timestamp of last time division
if(!file_exists($timestamp)){
	$fp = fopen($timestamp,'a+');	# create new timestamp if not exists
	fwrite($fp,time());
	fclose($fp);
}
$lasttimestamp = file_get_contents($timestamp);	  # get time since last timestamp
echo "last time stamp " . $lasttimestamp . "\n";
if ( time() - $lasttimestamp > 5 ){
	# get and save current oldcount
	echo "its been over a minute\n";    
	$usercount = count( explode('a', file_get_contents($filename)) ) - 1 ;
	file_put_contents($oldcount, $usercount);

	# wipe out usercount
	file_put_contents($filename, '');

	# new timestamp
	file_put_contents($timestamp, time());

}


$array = explode('a', file_get_contents($filename));
#print_r($array);
#var_export($array);

$usercount =  count($array) -1 ;
echo "usercount " . $usercount . "\n" ;


# output oldcount	
echo "output: " . file_get_contents($oldcount)	 . "\n" ;

?>
